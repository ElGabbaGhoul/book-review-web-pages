# `{"key": value}` is a "dictionary literal"
#  it creates a new dictionary object with the supplied key-value pairs
{
    "language": "Python"
}



#   A dictionary is a value that is an unordered collection of key-value pairs
print({
    "product": "Macbook",
    "in_stock": True,
    "price": 4000
})





#  A variable can have a dictionary assigned to it.




#  You can use the `print` function to see the contents of a dictionary


# To add an item to a dictionary: `d[key]=value`



# To get an item from a dictionary: `d[key]`


# To get an item from a dictionary using: `d.get(get)`


# What's the difference?
# The `d[key]` form will raise an error if the key does not exist


# The `d.get(key)` form will not raise an error if the key does not exist


# Get the length of the dictionary by using the `len` function